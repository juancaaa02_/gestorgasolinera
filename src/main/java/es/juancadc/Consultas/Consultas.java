package es.juancadc.Consultas;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import es.juancadc.Models.DTOs.RepostajeGasolinera;
import es.juancadc.Models.DTOs.RepostajeUsuario;


public class Consultas {

	//Variable encapsulada.
	private static DataSource origenDeDatos;
	
	//Aqui almacenamos el pool de conexion
	public Consultas (DataSource origenDatos) {
		
		this.origenDeDatos=origenDatos;
	
	}
	
	//Metodo para devolver listado de productos 
	


	public void addNuevoRepostaje(RepostajeUsuario nuevoRepostaje) {
		
		Connection miConexion=null;
		PreparedStatement miStatement=null;
		
		//Obtener la conexion
		
		try {
			miConexion=origenDeDatos.getConnection();
			
			//Crear intruccion sql de insert
			String sql="INSERT INTO repostajes (id,importe) VALUES (?,?)";
			
			miStatement=miConexion.prepareStatement(sql);
			
			//Establecer los parametro para el producto 
			
			miStatement.setInt(1, nuevoRepostaje.getId());
			miStatement.setString(2, nuevoRepostaje.getImporte());
		
			
			//Ejecutar la instruccion sql 
			
			miStatement.execute();

		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

	public void addNuevoRepostajeFactura(RepostajeUsuario nuevoRepostaje) {
	
		Connection miConexion=null;
		PreparedStatement miStatement=null;
		
		//Obtener la conexion
		
		try {
			miConexion=origenDeDatos.getConnection();
			
			//Crear intruccion sql de insert
			String sql="INSERT INTO repostajes (id,importe,dni,matricula) VALUES (?,?,?,?)";
			
			miStatement=miConexion.prepareStatement(sql);
			
			//Establecer los parametro para el producto 
			
			miStatement.setInt(1, nuevoRepostaje.getId());
			miStatement.setString(2, nuevoRepostaje.getImporte());
			miStatement.setString(3, nuevoRepostaje.getDni());
			miStatement.setString(4, nuevoRepostaje.getMatricula());
		
			
			//Ejecutar la instruccion sql 
			
			miStatement.execute();

		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
	}

	public List<RepostajeUsuario> selectRepostajes() throws Exception {
		
		List<RepostajeUsuario> repostajes = new ArrayList<>();
		
		//Creamos los objetos para la conexion 
		
		Connection miConexion=null;
		Statement miStatement=null;
		ResultSet miResulset = null;
		
		//Establecemos la conexion
		
		miConexion=origenDeDatos.getConnection();
		
		//Creamos la sentencia SQL y el Statement
		
		String sql="SELECT * FROM repostajes";
		miStatement=miConexion.createStatement();
		
		//Ejecutamos la sentencia
		
		miResulset=miStatement.executeQuery(sql);
		
		//Recorremos el resulset obtenido de la consulta
		
		while(miResulset.next()) {
			
			int id = miResulset.getInt(1);
			String importe = miResulset.getString(2);
			String dni = miResulset.getString(3);
			String matricula = miResulset.getString(4);
			
			RepostajeUsuario temRepos = new RepostajeUsuario(id, importe, dni, matricula);
			
			repostajes.add(temRepos);
		}
		
		
		return repostajes;
	}
	
	public List<RepostajeGasolinera> selectRepostajesGasolinera() throws Exception {

		List<RepostajeGasolinera> repostajes = new ArrayList<>();
		
		//Creamos los objetos para la conexion 
		
		Connection miConexion=null;
		Statement miStatement=null;
		ResultSet miResulset = null;
		
		//Establecemos la conexion
		
		miConexion=origenDeDatos.getConnection();
		
		//Creamos la sentencia SQL y el Statement
		
		String sql="SELECT * FROM repostajegasolinera";
		miStatement=miConexion.createStatement();
		
		//Ejecutamos la sentencia
		
		miResulset=miStatement.executeQuery(sql);
		
		//Recorremos el resulset obtenido de la consulta
		
		while(miResulset.next()) {
			
			int id = miResulset.getInt(1);
			String cantidad = miResulset.getString(2);
			String combustible = miResulset.getString(3);
			String importe = miResulset.getString(4);
			
			RepostajeGasolinera temRepos = new RepostajeGasolinera(id, cantidad, combustible, importe);
			
			
			repostajes.add(temRepos);
		}
		
		
		return repostajes;
	}

	public String calculoImporteTotal() throws Exception {
	//Creamos los objetos para la conexion 
		String importeTotal="";
		
		Connection miConexion=null;
		Statement miStatement=null;
		ResultSet miResulset = null;
		
		//Establecemos la conexion
		
		miConexion=origenDeDatos.getConnection();
		
		//Creamos la sentencia SQL y el Statement
		
				String sql="SELECT SUM(importe) FROM repostajes";
				miStatement=miConexion.createStatement();
				
				miResulset=miStatement.executeQuery(sql);
				
				while(miResulset.next()) {
					
					 importeTotal = miResulset.getString(1);
					
					
					
				}
		return importeTotal;
	}

	public List<String> calculoImporteTotal2() {
		// TODO Auto-generated method stub
		return null;
	}

	public void addNuevoRepostajeGasolinera(RepostajeGasolinera nuevoRepostaje) {
		
		Connection miConexion=null;
		PreparedStatement miStatement=null;
		
		//Obtener la conexion
		
		try {
			miConexion=origenDeDatos.getConnection();
			
			//Crear intruccion sql de insert
			String sql="INSERT INTO repostajegasolinera (idRepostaje,cantidad,combustible,importe) VALUES (?,?,?,?)";
			
			miStatement=miConexion.prepareStatement(sql);
			
			//Establecer los parametro para el producto 
			miStatement.setInt(1, nuevoRepostaje.getIdRepostaje());
			miStatement.setString(2, nuevoRepostaje.getCantidad());
			miStatement.setString(3, nuevoRepostaje.getTipoCombustible());
			miStatement.setString(4, nuevoRepostaje.getImporte());

			//Ejecutar la instruccion sql 
			
			miStatement.execute();

		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
	}

	public void elimanarProducto(String idRepostajeGasolinera) throws Exception {
		
		Connection miConexion=null;
		PreparedStatement miStatement=null;
		
		//Establecer conexion BBDD
		
			miConexion=origenDeDatos.getConnection();
			//Crear instruccion sql de elinar
			String sql="DELETE FROM repostajegasolinera WHERE idRepostaje=?";
			//Preparar la consulta 
			miStatement=miConexion.prepareStatement(sql);
			//Establecer los parametros de consulta
			miStatement.setString(1, idRepostajeGasolinera);
			//Ejecutar senetcnia sql
			
			miStatement.execute();
		
	}

	

	
	
	
}
