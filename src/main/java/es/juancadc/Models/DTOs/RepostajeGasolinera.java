package es.juancadc.Models.DTOs;

public class RepostajeGasolinera {

	//Campos 
	
	private int idRepostaje;
	
	private String cantidad;
	
	private String tipoCombustible;
	
	private String importe;


	//Getters and Setters
	
	public int getIdRepostaje() {
		return idRepostaje;
	}

	public void setIdRepostaje(int idRepostaje) {
		this.idRepostaje = idRepostaje;
	}
	
	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public String getTipoCombustible() {
		return tipoCombustible;
	}

	public void setTipoCombustible(String tipoCombustible) {
		this.tipoCombustible = tipoCombustible;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	
	//Constructores
	
	public RepostajeGasolinera(int idRepostaje, String cantidad, String tipoCombustible, String importe) {
		super();
		this.idRepostaje = idRepostaje;
		this.cantidad = cantidad;
		this.tipoCombustible = tipoCombustible;
		this.importe = importe;
	}
	
	
	
}
