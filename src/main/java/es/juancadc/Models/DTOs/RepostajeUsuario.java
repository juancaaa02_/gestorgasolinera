package es.juancadc.Models.DTOs;

public class RepostajeUsuario {

	//Campos
	
	private int id;
	private String importe;
	private String dni;
	private String matricula;
	
	//Getters and Setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getImporte() {
		return importe;
	}
	public void setImporte(String importe) {
		this.importe = importe;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	
	//Constructor para repostaje normal
	
	public RepostajeUsuario(int id, String importe) {
		super();
		this.id = id;
		this.importe = importe;
	}

	
	//Constructor para repostaje con factura
	
	public RepostajeUsuario(int id, String importe, String dni, String matricula) {
		super();
		this.id = id;
		this.importe = importe;
		this.dni = dni;
		this.matricula = matricula;
	}
	
	
}
