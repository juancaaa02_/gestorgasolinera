package es.juancadc.Controllers;

import jakarta.annotation.Resource;
import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.awt.Image;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

import javax.sql.DataSource;

import es.juancadc.Consultas.Consultas;
import es.juancadc.Models.DTOs.RepostajeGasolinera;
import es.juancadc.Models.DTOs.RepostajeUsuario;


/**
 * Servlet implementation class ControllersProductos
 */
public class Controllers extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Consultas consultaGestor;
	
	@Resource(name="jdbc/gasolinera")
	//Creamos una variable de DataSource
	private DataSource mipool;
	

	
	
	@Override
	public void init() throws ServletException {
		super.init();
		
		//Conectamos con el modelo consulta productos
		try {

			
			consultaGestor = new Consultas(mipool);
		
		}catch (Exception a) {
		
		throw new ServletException(a);
		
	}
}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//Leer el parametro que le llega del formulario.
		
		String elComando=request.getParameter("instruccion");
		
		
		//Si no se envia el parametro, listar los productos
		
		if(elComando==null) 
			elComando="paginaInicio";
		
		
		
		//Si ha resivido el parametro o no, redirigir el flujo al metodo adecuado 
		
		switch (elComando) {
		
		case "paginaInicio":
			
			cargarPaginaInicio(request,response);
			break;

		case "repostajeNormal":{
			
			repostajeNormal(request,response);
			
			break;
		}
		
		case "repostajeFactura":
			
			repostajeFactura(request,response);
			break;
		case "listadoRepostajes":
			
			listadoDeRepostajes(request,response);
			break;
			
		case "importeTotalCombustible":
			
			break;
			
		case "repostajeGasolinera":
			
			repostajeGasolinera(request,response);
			break;
			
		case"eliminarRepostaje":
			
			try {
				eliminarRepostajeGasolinera(request,response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		
		case "listadoRepostajeGasolinera":
			
			listadoRepostajeGasolinera(request,response);
			break;
		default:
			
			
		}
		
	}







private void cargarPaginaInicio(HttpServletRequest request, HttpServletResponse response) {
		
	RequestDispatcher miDispatcher=request.getRequestDispatcher("/paginaInicio.jsp");
	
	try {
		miDispatcher.forward(request, response);
	} catch (ServletException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}
private void repostajeNormal(HttpServletRequest request, HttpServletResponse response) {
		
		//Leer la informacion del producto que viene del formulario
		
		int id=Integer.parseInt(request.getParameter("id"));
		String importe=request.getParameter("importe");
		

		//Crear un objeto de tipo ProductoDTOs
		
		RepostajeUsuario nuevoRepostaje = new RepostajeUsuario(id, importe);
		
		
		//Enviar el objeto al modelo y despues insertart el objeto producto a la base de datos.
		
		consultaGestor.addNuevoRepostaje(nuevoRepostaje);
		
		//Volver a listar la tabla de productos.
		
		cargarPaginaInicio(request, response);
		
		
		
	}

private void repostajeFactura(HttpServletRequest request, HttpServletResponse response) {
	
	
	//Leer la informacion del producto que viene del formulario
	int id=Integer.parseInt(request.getParameter("id"));
	String importe=request.getParameter("importe");
	String dni=request.getParameter("dni");
	String matricula = request.getParameter("matricula");
	
	
	
	//Crear un objeto de tipo ProductoDTOs
	
	RepostajeUsuario nuevoRepostaje = new RepostajeUsuario(id, importe,dni,matricula);
	
	//Enviar el objeto al modelo y despues insertart el objeto producto a la base de datos.
	
	consultaGestor.addNuevoRepostajeFactura(nuevoRepostaje);
	
	//Volver a listar la tabla de productos.
	
	cargarPaginaInicio(request, response);
	
}

private void listadoDeRepostajes(HttpServletRequest request, HttpServletResponse response) {
	
	//Creamos la lista de repostajes
	List<RepostajeUsuario> listaRepostaje;
	
	try {
		
		//Almacenamos en la lista de repostajes todos los datos que nos devuelve el metodo selectRepostajes.
		
		listaRepostaje=consultaGestor.selectRepostajes();
		
		//Agregamos la lista obtenida al request 
		
		request.setAttribute("LISTAREPOSTAJES", listaRepostaje);
		
		//Enviamos el request a la pagina jsp
		
		RequestDispatcher dispacher = request.getRequestDispatcher("/listadoRepostajes.jsp");
		dispacher.forward(request, response);
		
		listadoDeRepostajes(request,response);
	
	} catch (Exception e) {

		e.printStackTrace();
	}

}
private void listadoRepostajeGasolinera(HttpServletRequest request, HttpServletResponse response) {
	
	//Creamos la lista de repostajes
		List<RepostajeGasolinera> listaRepostaje;
		
		try {
			
			//Almacenamos en la lista de repostajes todos los datos que nos devuelve el metodo selectRepostajes.
			
			listaRepostaje=consultaGestor.selectRepostajesGasolinera();
			
			//Agregamos la lista obtenida al request 
			
			request.setAttribute("LISTAREPOSTAJES", listaRepostaje);
			
			//Enviamos el request a la pagina jsp
			
			RequestDispatcher dispacher = request.getRequestDispatcher("/listadoLlenadosDepositos.jsp");
			dispacher.forward(request, response);
			
			listadoRepostajeGasolinera(request, response);
		
		} catch (Exception e) {

			e.printStackTrace();
		}
		
	}

private void importeTotal(HttpServletRequest request, HttpServletResponse response) {

	List<String> importeTotal;
	
	try {
	importeTotal=consultaGestor.calculoImporteTotal2();
	request.setAttribute("IMPORTETOTAL", importeTotal);
	//Enviamos el request a la pagina jsp
	
			RequestDispatcher dispacher = request.getRequestDispatcher("/importeTotal.jsp");
			dispacher.forward(request, response);
			
			importeTotal(request,response);
	}catch (Exception e) {
		e.printStackTrace();
	}

	}


private void repostajeGasolinera(HttpServletRequest request, HttpServletResponse response) {
	
	//Leer la informacion del producto que viene del formulario
		int idRepostaje =Integer.parseInt(request.getParameter("idRepostaje"));
		String cantidad=request.getParameter("cantidad");
		String combustible=request.getParameter("combustible");
		String importe=request.getParameter("importe");
		
		
		
		//Crear un objeto de tipo ProductoDTOs
		
		RepostajeGasolinera nuevoRepostaje = new RepostajeGasolinera(idRepostaje,cantidad, combustible, importe);
		
		//Enviar el objeto al modelo y despues insertart el objeto producto a la base de datos.
		
		consultaGestor.addNuevoRepostajeGasolinera(nuevoRepostaje);
		
		//Volver a listar la tabla de productos.
		
		cargarPaginaInicio(request, response);
	
}

private void eliminarRepostajeGasolinera(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
	//Capturar el codigo articulo del producto que queremos borrar
	String idRepostajeGasolinera=request.getParameter("idRepostaje");
	//Borrar producto de la base de datos
	
	consultaGestor.elimanarProducto(idRepostajeGasolinera);
	//Mostrar el lista sin el producto que hemos borrado
	cargarPaginaInicio(request,response);
}
	
}
	


